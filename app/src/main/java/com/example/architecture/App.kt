package com.example.architecture

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.multidex.MultiDex
import com.didi.drouter.api.DRouter
import com.example.common.export.app.IAppStartup

/**
资源样式 统一
数据库 下沉 统一库
组件公开库，项目复杂可以 为每一个模块配置一个expert， 放置公共的数据bean，接口，api，常量等
 */
class App : Application() {

    /**
    模块初始化
    各种组件初始化任务，需要评估项目复杂度 酌情而定。 大部分应用没有大厂应用那么复杂，不需要设计应用启动框架
    可能就几个启动组件，没有依赖关系，放在主线程，子线程都可。
    按照简单情况，利用路由组件 设计通信接口IAppStartup ， 只有路由组件需要 提前初始化，其他初始化任务放在路由后进行。
    路由支持service 设置优先级 ，也可以简单的设置任务执行顺序
    如果组件非常多，那就要考虑引入，启动任务框架了。

    如果为了规避应用合规审查的问题，可以把如下代码挪到 提示弹窗确认后运行

     */
    override fun onCreate() {
        super.onCreate()
        Thread {
            DRouter.init(this)
            val list = DRouter.build(IAppStartup::class.java).getAllService()
            for (service: IAppStartup in list) {
                service.doWork(this)
            }
        }.start()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}