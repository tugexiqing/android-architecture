package com.example.library.datasource

import com.tencent.mmkv.MMKV

object CacheDataSource {
    private var mmkv: MMKV = MMKV.defaultMMKV()

    fun create(id: String) {
        mmkv = MMKV.mmkvWithID(id)
    }

    fun putInt(key: String, value: Int) {
        mmkv.encode(key, value)
    }

    fun putString(key: String, value: String) {
        mmkv.encode(key, value)
    }

    fun putBoolean(key: String, value: Boolean) {
        mmkv.encode(key, value)
    }

    fun getInt(key: String, defaultValue: Int = 0): Int {
        return mmkv.decodeInt(key, defaultValue)
    }

    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean {
        return mmkv.decodeBool(key, defaultValue)
    }

    fun getString(key: String, defaultValue: String = ""): String {
        return mmkv.decodeString(key, defaultValue)!!
    }
}