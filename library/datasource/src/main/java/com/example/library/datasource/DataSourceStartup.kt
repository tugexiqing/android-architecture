package com.example.library.datasource

import android.app.Application
import com.didi.drouter.annotation.Service
import com.example.common.export.app.IAppStartup
import com.tencent.mmkv.MMKV


@Service(function = [IAppStartup::class], priority = 0)
class DataSourceStartup : IAppStartup {
    override fun doWork(context: Application) {
        MMKV.initialize(context)
    }
}