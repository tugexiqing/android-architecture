package com.example.library.datasource

interface IRemoteSetting {
    val isDebug: Boolean

    val baseUrl:String
}