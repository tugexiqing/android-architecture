package com.example.library.datasource

import android.util.ArrayMap
import com.google.gson.reflect.TypeToken

/**
 * 网络请求 参数收集者
 */
class ParameterCollector<T>  {
    val params = ArrayMap<String, Any>()
    val header = ArrayMap<String, String>()

    var requestType: RequestType = RequestType.Get
        private set
    var resultType: ResultType = ResultType.Object
        private set

    //某些情况下 body 想直接传递一个对象时使用 比map方便
    var body: Any? = null
        private set
    var url: String? = null
        private set
    var clazz: Class<T>? = null
        private set
    var typeToken: TypeToken<T>? = null
        private set

    class Builder<R> {
        private val params = ArrayMap<String, Any>()
        private val header = ArrayMap<String, String>()
        private var body: Any? = null
        private var typeToken: TypeToken<R>? = null
        private var clazz: Class<R>? = null
        private var requestType: RequestType = RequestType.Get
        private var resultType: ResultType = ResultType.Object

        fun putParams(key: String, value: Any): Builder<R> {
            params[key] = value
            return this
        }

        fun putHeader(key: String, value: String): Builder<R> {
            header[key] = value
            return this
        }

        fun setRequestType(type: RequestType): Builder<R> {
            this.requestType = type
            return this
        }

        fun setResultType(type: ResultType): Builder<R> {
            this.resultType = type
            return this
        }

        fun setClazz(clazz: Class<R>): Builder<R> {
            this.clazz = clazz
            return this
        }

        fun setBody(body: Any): Builder<R> {
            this.body = body
            return this
        }

        fun setTypeToken(typeToken: TypeToken<R>): Builder<R> {
            this.typeToken = typeToken
            return this
        }


        fun build(url: String): ParameterCollector<R> {
            val request = ParameterCollector<R>()
            request.url = url
            request.params.putAll(params)
            request.header.putAll(header)
            request.requestType = requestType
            request.body = body
            request.typeToken = typeToken
            request.clazz = clazz
//
//            if (clazz == null && typeToken == null) {
//                throw Exception("请设置 ParameterCollector 的 clazz 或 typeToken 属性")
//            }
            return request
        }
    }


    //    @IntDef(Type.GET, Type.POST_FORM, Type.POST_BODY, Type.POST_MULTIPART)
//    annotation class Type {
//        companion object {
//            const val GET = 0
//            const val POST_BODY = 1
//            const val POST_FORM = 2
//            const val POST_MULTIPART = 3
//        }
//    }
    sealed class RequestType {
        object Get : RequestType()
        object PostBody : RequestType()
        object PostForm : RequestType()
        object PostMultipart : RequestType()
    }

    sealed class ResultType {
        object Object : ResultType()
        object Array : ResultType()
    }
}