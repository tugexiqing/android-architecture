package com.example.common.database.impl

import com.example.common.database.DBDataSource
import com.example.common.database.dao.UserDao
import com.example.common.database.entity.DBUser

class UserDaoImpl {
    private var userDao: UserDao? = DBDataSource.getUserDao()

     suspend fun getAll(): List<DBUser>? {
        return userDao?.getAll()
    }

    suspend fun add(user: DBUser) {
        userDao?.add(user)
    }

    suspend fun getOne(id:Int):DBUser? {
        return userDao?.getOne(id)
    }
}