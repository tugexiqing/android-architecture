package com.example.common.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "db_user")
data class DBUser(
    @PrimaryKey var id: Int,
    @ColumnInfo(name = "name") var name: String,
    ) {
}