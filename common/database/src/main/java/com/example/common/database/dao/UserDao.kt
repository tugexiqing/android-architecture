package com.example.common.database.dao

import androidx.room.*
import com.example.common.database.entity.DBUser

@Dao
internal interface UserDao {


    @Query("select * from db_user")
    suspend fun getAll(): List<DBUser>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(user: DBUser)

    @Update
    suspend fun update(user: DBUser)

    @Query("select * from db_user where id = :id")
    suspend fun getOne(id: Int): DBUser
}