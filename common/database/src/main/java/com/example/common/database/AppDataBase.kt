package com.example.common.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.common.database.dao.UserDao
import com.example.common.database.entity.DBUser

@Database(entities = [DBUser::class], version = 1, exportSchema = false)
internal abstract class AppDataBase : RoomDatabase() {

    abstract fun userDao(): UserDao
}