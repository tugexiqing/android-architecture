package com.example.common.export.app

import android.app.Application

/**
 * 模块初始化接口
 */
interface IAppStartup {

    fun doWork(context:Application)
}