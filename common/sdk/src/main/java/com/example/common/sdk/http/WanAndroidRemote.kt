package com.example.common.sdk.http

import androidx.core.net.ParseException
import com.example.common.sdk.entity.UIResult
import com.example.library.datasource.ParameterCollector
import com.example.library.datasource.RemoteDataSource
import com.google.gson.JsonParseException
import org.json.JSONException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


object WanAndroidRemote : RemoteDataSource(WanAndroidSetting()) {
    private val service: ApiService by lazy {
        createService(ApiService::class.java)
    }


    suspend fun requestNoConvert(request: ParameterCollector<Nothing>): UIResult<Any> {
        return try {
            val httpEntity = request(request)
            if (httpEntity.code == 0) {
                UIResult.success(
                    httpEntity.code, httpEntity.data
                )
            } else {
                UIResult.error(httpEntity.code, httpEntity.msg)
            }
        } catch (e: Exception) {
            UIResult.error(500, handlerErrorMessage(e))
        }
    }

    suspend fun <T> requestObject(request: ParameterCollector<T>): UIResult<T> {
        try {
            val httpEntity = request(request)
            return if (httpEntity.code == 0) {
                UIResult.success(
                    httpEntity.code,
                    DataTransformProcessor.transformObject(
                        httpEntity.data,
                        request.clazz,
                        request.typeToken
                    )
                )
            } else {
                UIResult.error(httpEntity.code, httpEntity.msg)
            }
        } catch (e: Exception) {
            return UIResult.error(500, handlerErrorMessage(e))
        }
    }

    suspend fun <T> requestArray(request: ParameterCollector<T>): UIResult<List<T>> {
        try {
            val httpEntity = request(request)
            return if (httpEntity.code == 0) {
                UIResult.success(
                    httpEntity.code,
                    DataTransformProcessor.transformArray(
                        httpEntity.data,
                        request.clazz,
                        request.typeToken
                    )
                )
            } else {
                UIResult.error(httpEntity.code, httpEntity.msg)
            }
        } catch (e: Exception) {
            return UIResult.error(500, handlerErrorMessage(e))
        }
    }

    private suspend fun <T> request(request: ParameterCollector<T>): HttpEntity {
        return when (request.requestType) {
            ParameterCollector.RequestType.Get -> {
                service.get(request.url!!, request.params, request.header)
            }
            ParameterCollector.RequestType.PostBody -> {
                service.post(request.url!!, request.body!!, request.header)
            }
            ParameterCollector.RequestType.PostForm -> {
                service.post(request.url!!, request.params, request.header)
            }
            ParameterCollector.RequestType.PostMultipart -> {
                service.post(request.url!!, request.params, request.header)
            }
        }
    }

    private fun handlerErrorMessage(e: Exception): String {
        return when (e) {
            is UnknownHostException, is ConnectException -> {
                "网络不可用"
            }
            is SocketTimeoutException -> {
                "请求网络超时"
            }
            is JsonParseException, is ParseException, is JSONException -> {
                "数据解析错误"
            }
            else -> {
                "未知错误"
            }
        }
    }
}