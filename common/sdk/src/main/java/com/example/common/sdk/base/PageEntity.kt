package com.example.common.sdk.base

import com.google.gson.annotations.SerializedName

 data class PageEntity<T>(
    @SerializedName("curPage") var currPage: Int,
    @SerializedName("datas") var list: MutableList<T>,
    @SerializedName("pageCount") var pageCount: Int,
    @SerializedName("size") var size: Int,
    @SerializedName("total") var total: Int
){
     constructor():this(0, mutableListOf(),0,0,0)

     fun clone(page:PageEntity<*>?){
         page?.let {
             this.currPage = it.currPage
             this.pageCount = it.pageCount
             this.size = it.size
             this.total = it.size
         }

     }
 }