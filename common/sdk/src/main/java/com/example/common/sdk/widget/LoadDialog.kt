package com.example.common.sdk.widget

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.example.common.sdk.databinding.LibraryDialogLoadingBinding

class LoadDialog(context: Context) : Dialog(context) {

    private lateinit var mBinding: LibraryDialogLoadingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = LibraryDialogLoadingBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        window?.setDimAmount(0f)
    }

}