package com.example.common.sdk.http

import com.google.gson.annotations.SerializedName


data class HttpEntity(
    @SerializedName("data")
    val data: Any,
    @SerializedName("errorCode") val code: Int,
    @SerializedName("errorMsg") val msg: String
)
