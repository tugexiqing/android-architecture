package com.example.common.sdk.http

import com.example.common.sdk.BuildConfig
import com.example.library.datasource.IRemoteSetting

class WanAndroidSetting : IRemoteSetting {
    override val isDebug: Boolean
        get() = BuildConfig.DEBUG
    override val baseUrl: String
        get() = "https://www.wanandroid.com/"
}