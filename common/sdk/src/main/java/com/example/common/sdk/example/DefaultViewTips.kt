package com.example.common.sdk.example

import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.example.common.sdk.function.IUiTips
import com.example.common.sdk.widget.LoadDialog

/**
 * UI提示默认实现
 */
class DefaultViewTips(private var context: FragmentActivity?) : IUiTips, DefaultLifecycleObserver {
    private val dialog = LoadDialog(context!!)
    override fun showLoading() {
        dialog.show()
    }

    override fun hideLoading() {
        dialog.dismiss()
    }

    override fun toast(text: String) {
        context?.let {
            Toast.makeText(it, text, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy(owner: LifecycleOwner) {
        context = null
        super.onDestroy(owner)
    }
}