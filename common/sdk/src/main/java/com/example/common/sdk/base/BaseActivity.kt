package com.example.common.sdk.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.common.sdk.example.DefaultViewTips
import com.example.common.sdk.function.IUiTips

abstract class BaseActivity : AppCompatActivity(), IUiTips {

    private val mUiTips: IUiTips by lazy {
        val defaultViewTips = DefaultViewTips(this)
        lifecycle.addObserver(defaultViewTips)
        return@lazy defaultViewTips
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
     }

    override fun showLoading() {
        mUiTips.showLoading()
    }

    override fun hideLoading() {
        mUiTips.hideLoading()
    }

    override fun toast(text: String) {
        mUiTips.toast(text)
    }
}

