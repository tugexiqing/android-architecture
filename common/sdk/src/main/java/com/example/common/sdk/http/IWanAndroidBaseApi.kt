package com.example.common.sdk.http

import com.example.common.sdk.entity.UIResult
import kotlinx.coroutines.flow.*

interface IWanAndroidBaseApi {
}

fun <T> IWanAndroidBaseApi.flowWrapObject(block: suspend () -> UIResult<T>): Flow<UIResult<T>> {
    return flow {
        emit(block())
    }.catch { cause: Throwable ->
        cause.printStackTrace()
        emit(UIResult.error(500, cause.message ?: "发生了一些错误"))
    }.onStart {
        emit(UIResult.loading())
    }.onCompletion {
        emit(UIResult.completion())
    }
}

fun <T> IWanAndroidBaseApi.flowWrapList(block: suspend () -> UIResult<List<T>>): Flow<UIResult<List<T>>> {
    return flow {
        emit(block())
    }.catch { cause: Throwable ->
        cause.printStackTrace()
        emit(UIResult.error(500, cause.message ?: "发生了一些错误"))
    }.onStart {
        emit(UIResult.loading())
    }.onCompletion {
        emit(UIResult.completion())
    }
}