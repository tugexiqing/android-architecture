package com.example.common.sdk.function

/**
 *  UI提示接口
 */
interface IUiTips {
    fun showLoading()

    fun hideLoading()

    fun toast(text:String)
}