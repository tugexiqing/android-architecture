package com.example.common.sdk.entity


data class UIResult<T> internal constructor(
    val code: Int,
    val msg: String,
    val data: T? = null,
    val status: Status
) {
    companion object {

        fun <T> success(code: Int, data: T): UIResult<T> {
            return UIResult(code, "", data, Status.Success)
        }

        fun <T> loading(): UIResult<T> {
            return UIResult(-1, "", null, Status.Load)
        }

        fun <T> error(code: Int, msg: String): UIResult<T> {
            return UIResult(code, msg, null, Status.Error)
        }

        fun <T> completion(): UIResult<T> {
            return UIResult(-1, "", null, Status.Completion)
        }
    }

    fun <T> clone(data: T): UIResult<T> {
        return UIResult<T>(
            code,
            msg,
            data,
            status
        )
    }

    sealed class Status {
        object Load : Status()
        object Success : Status()
        object Error : Status()
        object Completion : Status()
    }
}
