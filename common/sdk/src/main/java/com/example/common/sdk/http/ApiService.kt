package com.example.common.sdk.http

import androidx.collection.ArrayMap
import okhttp3.MultipartBody
import retrofit2.http.*

interface ApiService {

    @GET
    suspend fun get(
        @Url url: String,
        @QueryMap params: Map<String, @JvmSuppressWildcards Any> = ArrayMap(),
        @HeaderMap header: Map<String, String> = ArrayMap()
    ): HttpEntity

    @FormUrlEncoded
    @POST
    suspend fun post(
        @Url url: String,
        @FieldMap params: Map<String, @JvmSuppressWildcards Any> = ArrayMap(),
        @HeaderMap header: Map<String, String> = ArrayMap()
    ): HttpEntity

    @POST
    suspend fun post(
        @Url url: String,
        @Body body: Any,
        @HeaderMap header: Map<String, String> = ArrayMap()
    ): HttpEntity

    @POST
    suspend fun post(
        @Url url: String,
        @Part parts: List<MultipartBody.Part>,
        @HeaderMap header: Map<String, String> = ArrayMap()
    ): HttpEntity

}