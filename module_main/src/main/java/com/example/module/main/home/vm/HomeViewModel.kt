package com.example.module.main.home.vm

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.sdk.base.PageEntity
import com.example.common.sdk.extend.UIResultLiveData
import com.example.common.sdk.extend.UIResultMutableLiveData
import com.example.module.main.home.data.domain.HomeArticleListUseCase
import com.example.module.main.home.data.mapper.ArticleMapper
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {
    private val useCase: HomeArticleListUseCase by lazy { HomeArticleListUseCase() }
    private val ldArticleList: UIResultMutableLiveData<PageEntity<ArticleMapper>> by lazy {
        UIResultMutableLiveData<PageEntity<ArticleMapper>>()
    }
    val onQueryArticleListResult: UIResultLiveData<PageEntity<ArticleMapper>> by lazy {
        ldArticleList
    }

    fun queryArticleList(pageIndex: Int) {
        viewModelScope.launch {
            useCase.queryArticleList(pageIndex, 20)
                .collect {
                    ldArticleList.value = it
                }
        }
    }

}