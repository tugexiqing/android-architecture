package com.example.module.main.home.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.module.main.R
import com.example.module.main.home.data.mapper.ArticleMapper

class HomeArticleAdapter :
    BaseQuickAdapter<ArticleMapper, BaseViewHolder>(R.layout.main_item_home_article),LoadMoreModule {

    override fun convert(holder: BaseViewHolder, item: ArticleMapper) {
        holder
            .setText(R.id.item_tv_title, item.title)
            .setText(R.id.item_tv_summary, item.summary)
    }
}