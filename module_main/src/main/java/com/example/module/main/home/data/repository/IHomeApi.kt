package com.example.module.main.home.data.repository

import com.example.common.sdk.base.PageEntity
import com.example.common.sdk.entity.UIResult
import com.example.common.sdk.http.IWanAndroidBaseApi
import com.example.module.main.home.data.entity.ArticleEntity
import kotlinx.coroutines.flow.Flow

interface IHomeApi:IWanAndroidBaseApi {

    suspend fun queryArticleList(page:Int,size:Int):Flow<UIResult<PageEntity<ArticleEntity>>>
}