package com.example.module.main.home

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import com.didi.drouter.annotation.Router
import com.example.common.sdk.base.BaseActivity
import com.example.common.sdk.extend.observerWithResult
import com.example.module.main.databinding.MainActivityHomeBinding
import com.example.module.main.home.view.HomeArticleList
import com.example.module.main.home.vm.HomeViewModel

@Router(path = "/main/home")
class HomeActivity : BaseActivity(), HomeArticleList.OnArticleListEventListener {
    private lateinit var mBinding: MainActivityHomeBinding
    private lateinit var modelHome: HomeViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = MainActivityHomeBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mBinding.rvList.onEventListener = this

        modelHome = ViewModelProvider(this)[HomeViewModel::class.java]
        //网络请求回调
        modelHome.onQueryArticleListResult.observerWithResult(this, this,
            onSuccess = {
                mBinding.rvList.bindData(page = it)
            }, onError = { code: Int, msg: String ->
                toast(msg)
                mBinding.rvList.loadMoreFail()
            })
    }

    /**
     * 列表加载数据
     */
    override fun onLoadDataEvent(currPage: Int) {
        modelHome.queryArticleList(currPage)
    }
}