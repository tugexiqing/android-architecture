package com.example.module.main.home.data.mapper

data class ArticleMapper(val id: Int, val title: String, val summary: String,val link:String)