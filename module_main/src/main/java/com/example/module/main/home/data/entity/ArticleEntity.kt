package com.example.module.main.home.data.entity

data class ArticleEntity(
    val id: Int,
    val title: String,
    val author: String,
    val shareUser: String,
    val collect: Boolean,
    val link: String,
    val niceShareDate: String
)