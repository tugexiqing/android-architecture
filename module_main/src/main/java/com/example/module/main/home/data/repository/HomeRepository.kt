package com.example.module.main.home.data.repository

import com.example.common.sdk.base.PageEntity
import com.example.common.sdk.entity.UIResult
import com.example.common.sdk.extend.genericType
import com.example.common.sdk.http.WanAndroidRemote
import com.example.common.sdk.http.flowWrapObject
import com.example.library.datasource.ParameterCollector
import com.example.module.main.home.data.entity.ArticleEntity
import kotlinx.coroutines.flow.Flow

class HomeRepository : IHomeApi {

    private val remoteDataSource = WanAndroidRemote

    override suspend fun queryArticleList(
        pageIndex: Int,
        size: Int
    ): Flow<UIResult<PageEntity<ArticleEntity>>> {
        val url = "article/list/${pageIndex}/json"
        val parameterCollector = ParameterCollector.Builder<PageEntity<ArticleEntity>>()
            .setRequestType(ParameterCollector.RequestType.Get)
            .setResultType(ParameterCollector.ResultType.Object)
            .putParams("page_size", size)
            .setTypeToken(genericType<PageEntity<ArticleEntity>>())
            .build(url)
        return flowWrapObject {
            remoteDataSource.requestObject(parameterCollector)
        }
    }
}