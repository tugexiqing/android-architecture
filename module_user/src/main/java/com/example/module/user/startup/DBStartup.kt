package com.example.module.user.startup

import android.app.Application
import com.didi.drouter.annotation.Service
import com.example.common.database.DBDataSource
import com.example.common.export.app.IAppStartup

@Service(function = [IAppStartup::class], priority = 0)
class DBStartup:IAppStartup {
    override fun doWork(context: Application) {
        DBDataSource.openDB(context, "default")
    }

}