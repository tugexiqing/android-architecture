package com.example.module.user.register

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.common.sdk.base.BaseActivity
import com.example.common.sdk.extend.observerWithResult
import com.example.module.user.databinding.UserActivityRegisterBinding
import com.example.module.user.databinding.UserSubRegisterViewBinding
import com.example.module.user.register.view.RegisterView
import com.example.module.user.register.vm.RegisterViewModel

/**
 * 所有view 都从activity中分离
 */
class RegisterActivity : BaseActivity(), RegisterView.RegisterViewEventListener {
    private lateinit var mBinding: UserActivityRegisterBinding
    private lateinit var registerModel: RegisterViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = UserActivityRegisterBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mBinding.root.eventListener = this

        registerModel = ViewModelProvider(this)[RegisterViewModel::class.java]
        registerModel.onRegisterResult.observerWithResult(this, this, true, onSuccess = {
            toast("注册成功")
            finish()
        })
    }

    override fun onSendRegisterEvent(userName: String, password: String, rePassword: String) {
        //原则上 activity 不应该出现判空提示。 判空属于逻辑，提示属于UI，应该参照Login的判空
        //判断写在viewModel，通过LiveData 发送事件到Activity进行提示
        if (TextUtils.isEmpty(userName)) {
            toast("请输入账户")
        } else if (TextUtils.isEmpty(password)) {
            toast("请输入密码")
        } else if (TextUtils.isEmpty(rePassword)) {
            toast("请再次输入密码")
        } else if (rePassword != password) {
            toast("两次密码输入不一致")
        } else {
            registerModel.register(userName, password, rePassword)
        }
    }
}