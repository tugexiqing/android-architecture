package com.example.module.user.register.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.sdk.extend.UIResultLiveData
import com.example.common.sdk.extend.UIResultMutableLiveData
import com.example.module.user.register.repository.RegisterRepository
import kotlinx.coroutines.launch

class RegisterViewModel : ViewModel() {
    private val repository: RegisterRepository by lazy { RegisterRepository() }
    private val ldRegister = UIResultMutableLiveData<Any>()
    val onRegisterResult: UIResultLiveData<Any> = ldRegister
    fun register(userName: String, password: String, rePassword: String) {
        viewModelScope.launch {
            repository.register(userName, password, rePassword)
                .collect {
                    ldRegister.value = it
                }
        }
    }
}