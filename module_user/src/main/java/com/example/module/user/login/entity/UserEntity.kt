package com.example.module.user.login.entity

import com.google.gson.annotations.SerializedName

data class UserEntity(
    @SerializedName("id")
    val id:Int,
    @SerializedName("nickname")
    val name:String)
