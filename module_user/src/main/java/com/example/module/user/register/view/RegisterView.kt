package com.example.module.user.register.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.widget.LinearLayoutCompat
import com.example.module.user.databinding.UserSubRegisterViewBinding

class RegisterView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayoutCompat(context, attrs) {
    private lateinit var mBinding: UserSubRegisterViewBinding
    var eventListener: RegisterViewEventListener? = null

    override fun onFinishInflate() {
        super.onFinishInflate()
        mBinding = UserSubRegisterViewBinding.inflate(LayoutInflater.from(context), this)
        mBinding.btnRegister.setOnClickListener {
            val userName = mBinding.etUserName.text.toString().trim()
            val password = mBinding.etPassword.text.toString().trim()
            val rePassword = mBinding.etRePassword.text.toString().trim()
            eventListener?.onSendRegisterEvent(userName, password, rePassword)

        }
    }

    interface RegisterViewEventListener {
        fun onSendRegisterEvent(userName: String, password: String, rePassword: String)
    }
}