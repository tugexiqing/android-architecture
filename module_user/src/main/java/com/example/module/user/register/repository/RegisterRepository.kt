package com.example.module.user.register.repository

import com.example.common.sdk.http.WanAndroidRemote
import com.example.common.sdk.http.flowWrapObject
import com.example.common.sdk.entity.UIResult
import com.example.library.datasource.ParameterCollector
import kotlinx.coroutines.flow.Flow

class RegisterRepository:IRegisterApi {
    private val remoteDataSource = WanAndroidRemote

    override suspend fun register(userName: String, password: String, rePassword: String): Flow<UIResult<Any>> {
        val parameterCollector = ParameterCollector.Builder<Nothing>()
            .setRequestType(ParameterCollector.RequestType.PostForm)
            .setResultType(ParameterCollector.ResultType.Object)
            .putParams("username", userName)
            .putParams("password", password)
            .putParams("repassword", rePassword)
            .build("user/register")
        return flowWrapObject { remoteDataSource.requestNoConvert(parameterCollector) }
    }
}