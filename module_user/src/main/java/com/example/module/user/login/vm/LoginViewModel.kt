package com.example.module.user.login.vm

import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.sdk.extend.UIResultLiveData
import com.example.common.sdk.extend.UIResultMutableLiveData
import com.example.module.user.login.repository.LoginRepository
import com.example.module.user.login.entity.UserEntity
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {
    private val repository: LoginRepository by lazy { LoginRepository() }
    private val ldLogin: UIResultMutableLiveData<UserEntity> by lazy { UIResultMutableLiveData() }
    private val ldToast :MutableLiveData<String> by lazy { MutableLiveData() }
    val onLoginResult: UIResultLiveData<UserEntity> = ldLogin
    val onToastResult: LiveData<String> = ldToast
    fun login(userName: String, password: String) {
        if (TextUtils.isEmpty(userName)) {
            ldToast.value = "请输入账户"
        } else if (TextUtils.isEmpty(password)) {
            ldToast.value = "请输入密码"
        } else {
            viewModelScope.launch {
                repository.login(userName, password)
                    .collect {
                        ldLogin.value = it
                    }
            }
        }
    }

    fun getCacheUserName():String{
        return repository.getCacheUserName()
    }


    fun getCachePassword():String{
        return repository.getCachePassword()
    }


}