package com.example.module.user.register.repository

import com.example.common.sdk.http.IWanAndroidBaseApi
import com.example.common.sdk.entity.UIResult
import kotlinx.coroutines.flow.Flow

interface IRegisterApi:IWanAndroidBaseApi {

    suspend fun  register(userName: String, password: String, rePassword: String):Flow<UIResult<Any>>
}