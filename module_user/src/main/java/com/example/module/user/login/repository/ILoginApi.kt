package com.example.module.user.login.repository

import com.example.common.sdk.http.IWanAndroidBaseApi
import com.example.common.sdk.entity.UIResult
import com.example.module.user.login.entity.UserEntity
import kotlinx.coroutines.flow.Flow

/**
 * LoginRepository接口
 */
interface ILoginApi : IWanAndroidBaseApi {
    suspend fun login(userName: String, password: String): Flow<UIResult<UserEntity>>

    fun getCacheUserName(): String

    fun getCachePassword(): String
}